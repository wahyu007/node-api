require('dotenv').config()

const express = require('express');
const app = express();
const mongoose = require('mongoose');

const port = 8000;

mongoose.connect(process.env.DATABASE_URL, { 
        useNewUrlParser : true,
        useUnifiedTopology : true 
    })
const db = mongoose.connection

db.on('error', (error) => {
    console.log(error)
})
db.once('open', () => {
    console.log('connected to database')
})

app.use(express.urlencoded({ extended : false}))
app.use(express.json())

const subscribersRouter = require('./routes/subscribers')
app.use('/subscribers', subscribersRouter)

// app.get('/', (req, res) => {
//     res.send("su")
// })

app.listen(port, (req,res) => {
    console.log(`Listen on port ${port} !`)
});