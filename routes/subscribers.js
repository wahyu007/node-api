const express = require('express');
const { update } = require('../models/subscriber');
const router = express.Router();
const Subscriber = require('../models/subscriber')

router.get('/', async (req, res) => {
    try {
        const subscribers = await Subscriber.find()
        res.json(subscribers)        
    } catch (err) {
        console.log("hallo error")
        res.status(500).json({ message : err.message })
    }
});

router.post('/', async (req, res) => {
    const subscriber = new Subscriber({
        name : req.body.name,
        subscribedChannel : req.body.subscribedChannel
    });

    try{
        const newSubscriber = await subscriber.save()
        res.status(201).json(newSubscriber)
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
})

router.get('/:id', getSubscriber, (req, res) => {
    console.log('tes')
    res.json(res.subscriber)
})

router.delete('/:id', getSubscriber, async (req, res) => {
    try {
        await res.subscriber.remove()
        res.json({
            message : 'Deleted This Subscriber'
        })
    } catch(err) {
        res.status(500).json({
            message : err.message
        })
    }
})

router.patch('/:id', getSubscriber, async (req, res) => {
    if(req.body.name != null){
        res.subscriber.name = req.body.name
    }

    if(req.body.subscribedChannel != null){
        res.subscriber.subscribedChannel = req.body.subscribedChannel
    }

    try {
        const updatedSubscriber = await res.subscriber.save()
        res.json(updatedSubscriber)
    } catch {
        res.status(400).json({
            message : err.message
        })
    }
})

async function getSubscriber(req, res, next) {
    try {
        subscriber = await Subscriber.findById(req.params.id)
        if(Subscriber == null){
            return res.status(404).json({
                message : 'Cant find subscriber'
            })
        }
    } catch (err) {
        console.log('tes yaa')
        return res.status(500).json({
            message : err.message
        })
    }
    res.subscriber = subscriber
    next()
}

module.exports = router;